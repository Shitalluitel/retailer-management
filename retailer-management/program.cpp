#include<iostream>
#include<process.h>
#include<fstream>
#include<conio.h>
#include<string>
#include<ctime>
#include <windows.h>
#include<process.h>
using namespace std;
char ch;
int x,y,go=0,sn,array_size=0,day,month,year;
char choice1[2],choice[2],choice2[2];
static int flag=0;
void wait(int);
void set_border();
void display_anime(int);
void set_cursor(int ,int);
class admin{
	private:
		string username;
		string password;
	protected:
		string name_of_file;
		int no_of_data;
	public:
		void get_user_pw();
		int check();
		void get_data(){
            ifstream data_file;
            data_file.open("data.dat");
			if(data_file.is_open()){
                while(!data_file.eof()){
					data_file>>username>>password>>name_of_file>>no_of_data;
				}
			}
			data_file.close();
		}
		void enter_data();
		void add_data();
		int check_new();
};
class user:public admin{
    string item;
  	string temp_name[300];
	int no_of_item,temp_number[300];
	double price_of_item,total_price;
	int no_of_list[20],total_item_purched;
	public:
		user(){
			for(int i=0;i<20;i++){
				no_of_list[i]=15*i;
			}
			total_item_purched=total_price=0;
			get_data();
        }
		void calculate();
        void check_list();
        int display(int,int);
        void billing();
        void update_record();
		void display_update();
};

void wait(int n){
    for(int i=0;i<=n;i++){
		Sleep (1000);
		if( _kbhit()){
			getch();
			set_border();
			return;
	     	}
	}
}
int user::display(int c,int s){
    string temp_item,temp_no_of_item,temp_price_of_item;
    set_border();
	x=3,y=2;
	sn=0;
	if(c==0){//to minimize error of displaying same data twice
		if((s-1)<0){
            set_border();
			display_anime(2);
			set_cursor(35,11);
			cout<<"NO DATA TO DISPLAY";
			wait(15);
			int temp;
			temp=display(1,1);
			return temp;
		}
		flag=s;
	}
	else{
		flag=0;
	}
	ifstream output;
	output.open(name_of_file.c_str());
    set_cursor(x,y++);
	cout<<".---------------------------------------------.";
	set_cursor(x,y++);
	cout<<"| SN |        Item        | Number Of | Price |";
	set_cursor(x,y++);
	cout<<"|    |                    |    Item   |(/item)|";
	set_cursor(x,y++);
	cout<<"|----|----------------------------------------|";
	set_cursor(x,y);
	cout<<"|    |                    |           |       |";
	set_cursor(x,y+1);
	cout<<"'----'----------------------------------------'";
   	while(!output.eof()){
		sn++;
		output>>temp_item>>temp_no_of_item>>temp_price_of_item;
		y++;
		if(y!=6 && sn>no_of_list[s-1] && sn<=no_of_list[s]){
		    set_cursor(x,y-1);
			cout<<"|    ";
			set_cursor(x+2,y-1);
			cout<<sn;
			set_cursor(x+5,y-1);
			cout<<"|                    ";
			set_cursor(x+7,y-1);
			cout<<temp_item;
			set_cursor(x+26,y-1);
			cout<<"|           ";
			set_cursor(x+28,y-1);
			cout<<temp_no_of_item;
			set_cursor(x+38,y-1);
			cout<<"|       ";
			set_cursor(x+40,y-1);
			cout<<temp_price_of_item;
			set_cursor(x+46,y-1);
			cout<<"|";
			set_cursor(x,y);
			cout<<"'----'----------------------------------------'";
		}
		if(sn%15==0 && sn!=0 && c==1){
			flag+=1;
		}
	  	if(y==21){
			y=6;
			if(sn==no_of_list[s]){
                output.close();
				return flag;
			}
		}
   }
   if(flag==20 || s<=0){
		flag=-1;
   }
   output.close();
   return flag+1;
}
//to display the list
void user::check_list(){
	x=3,y=3;
	int no;
	sn=0;
	go=display(1,1);
	do{
	   	x=53;
		y=7;
		set_cursor(x,y++);
		cout<<"1. MAIN MENU";
		set_cursor(x,y++);
		cout<<"2. PREVIOUS";
		set_cursor(x,y++);
		cout<<"3. NEXT";
	    set_cursor(x,y++);
		cout<<"4. SELECT";
		set_cursor(x,y++);
		cout<<"5. BILLING";
		set_cursor(x-1,y);
		cout<<"                          ";
		set_cursor(x,y++);
		cout<<"CHOICE: ";
		cin>>choice;
		set_cursor(x,y-1);
		cout<<"                        ";
		switch(choice[0]){
			case '1':
				set_border();
				flag=0;
				go=0;
             	if(check()){
					return ;
             	}
             	go=display(1,1);
             	break;
			case '2':
				go=display(0,go-1);
				set_cursor(55,19);
				break;
			case '3':
                go=display(1,go+1);
                set_cursor(55,20);
	            break;
			case '4':
				calculate();
				break;
			case '5':
				billing();
   				wait(10);
				go=display(1,go+1);
				break;
			default :
				break;
		}
	}while(1);
}
void user::billing(){
	set_border();
    for(int j=0;j<array_size;j++){
        ifstream input;
		input.open(name_of_file.c_str());
		ofstream tempfile;
		tempfile.open("temp.dat");
		while(!input.eof()){
            input>>item>>no_of_item>>price_of_item;
            if(item==temp_name[j]){
				no_of_item -=temp_number[j];
				if(no_of_item <0){
                	no_of_item +=temp_number[j];
                    set_border();
                    display_anime(1);
                    system("color 4f");
                	set_cursor(30,11);
                	cout<<"\aINVALID SELECTION MADE";
                	input.close();
                	tempfile.close();
                  	remove("temp.dat");
                	return ;
				}
            }
            tempfile<<"\n"<<item<<" "<<no_of_item<<" "<<price_of_item;
		}
		input.close();
		tempfile.close();
		ifstream input1;
		input1.open("temp.dat");
		ofstream tempfile1;
		tempfile1.open(name_of_file.c_str());
		while(!input1.eof()){
			input1>>item>>no_of_item>>price_of_item;
			tempfile1<<"\n"<<item<<" "<<no_of_item<<" "<<price_of_item;
		}
		tempfile1.close();
		input1.close();
	}
	set_cursor(5,3);
	cout<<"Total number of item Purched: "<<total_item_purched;
	set_cursor(5,4);
	cout<<"Total cost:                   "<<total_price;
	set_cursor(5,5);
	cout<<"List of item are: ";
	x=5;y=5;
	for(int i=0;i<array_size;i++){
		set_cursor(x,++y);
		cout<<i+1<<". "<<temp_name[i];
		if(y==22){
			x=x+22;
			y==5;
		}
		if(x==49 && y==22){
			x=y=5;
		}
	}
	remove("temp.dat");
	flag=sn=go=total_item_purched=total_price=array_size=0;
}
void user::calculate(){
    x=52;
	y=12;
	string temp;
	int no,f=0;
    set_cursor(x,y);
	cout<<"Item: ";
	cin>>temp;
	set_cursor(x,y);
	cout<<"                          ";
	set_cursor(x,y);
	cout<<"Number of item: ";
	cin>>no;
 	ifstream input;
	input.open(name_of_file.c_str());
	while(!input.eof()){
		input>>item>>no_of_item>>price_of_item;
		if(temp == item){
			temp_name[array_size]=item;
			temp_number[array_size]=no;
			array_size++;
			f=1;
			total_item_purched += no;
			total_price += no*price_of_item;
		}
	}
	if(f==0){
		set_border();
		display_anime(1);
		set_cursor(30,11);
		cout<<"\aINVALID ITEM NAME";
		wait(5);
		set_border();
		
	}
	input.close();
}
void admin::get_user_pw(){
	set_cursor(35,9);
	cout<<"Username: ";
	cin>>username;
	set_cursor(35,11);
	cout<<"Password: ";
	cin>>password;
	name_of_file="file.dat";
	ofstream output;
	output.open("data.dat");
	output<<username<<"\n"<<password<<"\n"<<name_of_file;
	output.close();
}
void display_anime(int n){
    x=15;y=2;
    set_cursor(x,y++);
	cout<<"                        .-'''-.";set_cursor(x,y++);
	cout<<"                       / .===. \\";set_cursor(x,y++);
	cout<<"                       \\/ 6 6 \\/";set_cursor(x,y++);
	if(n==1){
    cout<<"                       (   o   )";set_cursor(x,y++);
	}
	else{
	cout<<"                       ( \\___/ )";set_cursor(x,y++);
	}
	cout<<"  _______________ooo____\\_____/____ooo_______________";set_cursor(x,y++);
	cout<<" |                                                   |";set_cursor(x,y++);
	cout<<" |                                                   |";set_cursor(x,y++);
	cout<<" |                                                   |";set_cursor(x,y++);
	cout<<" |                                                   |";set_cursor(x,y++);
	cout<<" |                                                   |";set_cursor(x,y++);
	cout<<" |                                                   |";set_cursor(x,y++);
	cout<<" |                                                   |";set_cursor(x,y++);
	cout<<" |                                                   |";set_cursor(x,y++);
    cout<<" |                                                   |";set_cursor(x,y++);
	cout<<" |___________________________________________________|";set_cursor(x,y++);
	cout<<"                        |__|__|";set_cursor(x,y++);
	cout<<"                        /_'Y'_\\";set_cursor(x,y++);
	cout<<"                       (__/ \\__)";
}
int admin:: check(){
    string temp_user,temp_pass;
    display_anime(2);
	set_cursor(32,10);
	cout<<"Username: ";
	cin>>temp_user;
	set_cursor(32,12);
	cout<<"Password: ";
	cin>>temp_pass;
	if(temp_user==username && temp_pass==password){
		return 1;
	}
	set_border();
	display_anime(1);
	system("color 4f");
	set_cursor(34,11);
	cout<<"Incorrect Password\a";
	wait(20);
	system("color 2f");
	return 0;
}
void admin:: enter_data(){
	ofstream input;
    input.open(name_of_file.c_str());
    string item, no_of_item, price_of_item;
	int x=3,y=3,sn=0,no_of_data,i=0;
	set_border();
	set_cursor(7,12);
	cout<<"Enter the number of data that you want to insert:";
	cin>>no_of_data;
	set_border();
	set_cursor(x,y++);
	cout<<".---------------------------------------------.";
	set_cursor(x,y++);
	cout<<"| SN |        Item        | Number Of | Price |";
	set_cursor(x,y++);
	cout<<"|    |                    |    Item   |(/item)|";
	set_cursor(x,y++);
	cout<<"|----|----------------------------------------|";
	while(i<no_of_data){
        if(y<22){
			set_cursor(x,y);
			cout<<"|    |                    |           |       |";
			set_cursor(x,y+1);
			cout<<"'----'----------------------------------------'";
		}
		set_cursor(x,y);
		cout<<"|"<<++sn;
		set_cursor(x+5,y);
		cout<<"| ";
		cin>>item;
		set_cursor(x+26,y);
		cout<<"| ";
		cin>>no_of_item;
		set_cursor(x+38,y);
		cout<<"| ";
		cin>>price_of_item;
		input<<"\n"<<item<<" "<<no_of_item<<" "<<price_of_item;
		y++;
		i++;
		if(y==22){
			y=3;
			set_border();
		}
	}
	input.close();
}
void admin:: add_data(){
	fstream input;
    input.open(name_of_file.c_str(), ios::in | ios::out | ios::app );
    string item,no_of_item,price_of_item;
	int x=3,y=3,sn=0,no_of_data,i=0;
	set_border();
	set_cursor(7,12);
	cout<<"Enter the number of data that you want to insert:";
	cin>>no_of_data;
	set_border();
	set_cursor(x,y++);
	cout<<".---------------------------------------------.        ";
	set_cursor(x,y++);
	cout<<"| SN |        Item        | Number Of | Price |        ";
	set_cursor(x,y++);
	cout<<"|    |                    |    Item   |(/item)|";
	set_cursor(x,y++);
	cout<<"|----|----------------------------------------|";
	while(i<no_of_data){
		set_cursor(x,y);
		cout<<"|    |                    |           |       |";
		set_cursor(x,y+1);
		cout<<"'----'----------------------------------------'";
		set_cursor(x,y);
		cout<<"|"<<++sn;
		set_cursor(x+5,y);
		cout<<"| ";
		cin>>item;
		set_cursor(x+26,y);
		cout<<"| ";
		cin>>no_of_item;
		set_cursor(x+38,y);
		cout<<"| ";
		cin>>price_of_item;
		input<<"\n"<<item<<" "<<no_of_item<<" "<<price_of_item;
		y++;
		i++;
		if(y==22){
			y=3;
			set_border();
		}
	}
	input.close();
}
void user::display_update(){
    x=3,y=3;
	int no;
	sn=0;
	go=display(1,go+1);
	do{
	   	x=52;
		y=7;
		set_cursor(x,y++);
		cout<<"1. Back                 ";
		set_cursor(x,y++);
		cout<<"2. Previous             ";
		set_cursor(x,y++);
		cout<<"3. Next                 ";
	    set_cursor(x,y++);
		cout<<"4. Update               ";
		set_cursor(x,y);
		cout<<"                        ";
		set_cursor(x,y++);
		cout<<"Choice: ";
		cin>>choice;
		set_cursor(x,y-1);
		cout<<"                        ";
		switch(choice[0]){
			case '1':
				set_border();
			    flag=0;
			    go=0;
             	return;
			case '2':
				go=display(0,go-1);
				break;
			case '3':
                go=display(1,go+1);
	            break;
			case '4':
				update_record();
				break;
			default :
				break;
		}
	}while(1);

}
void user:: update_record(){
	int flag=1;
	string temp_item, temp_no_of_item,temp_price_of_item;
	ifstream input;
	input.open(name_of_file.c_str());
	ofstream output;
	output.open("temp.dat");
	x=52;
	y=7;
	set_cursor(x,y++);
	cout<<"             ";
	set_cursor(x,y++);
	cout<<"              ";
	set_cursor(x,y++);
	cout<<"               ";
	set_cursor(x,y++);
	cout<<"             ";
	set_cursor(x,y);
	cout<<"             ";
	set_cursor(x,y++);
	cout<<"             ";
	set_cursor(x,y-1);
	cout<<"             ";
	y=7;
	set_cursor(x,y++);
	cout<<"Item: ";
	cin>>temp_item;
	y=7;
    set_cursor(x,y++);
    cout<<"                   ";
	while(!input.eof()){
		input>>item>>no_of_item>>price_of_item;
		if(item == temp_item){
            y=7;
			flag=1;
            set_cursor(x,y++);
			cout<<"Item: ";
			cin>>temp_item;
            set_cursor(x,y++);
			cout<<"Quantity: ";
			cin>>temp_no_of_item;
			set_cursor(x,y++);
			cout<<"Price: ";
			cin>>temp_price_of_item;
			output<<"\n"<<temp_item<<" "<<temp_no_of_item<<" "<<temp_price_of_item;
			continue;
		}
		output<<"\n"<<item<<" "<<no_of_item<<" "<<price_of_item;
	}
	input.close();
	output.close();
	ifstream input1;
	input1.open("temp.dat");
	ofstream tempfile1;
	tempfile1.open(name_of_file.c_str());
	while(!input1.eof()){
		input1>>item>>no_of_item>>price_of_item;
		tempfile1<<"\n"<<item<<" "<<no_of_item<<" "<<price_of_item;
	}
	tempfile1.close();
	input1.close();
	remove("temp.dat");
}
inline void set_cursor(int x_coordinate, int y_coordinate){
	HANDLE hout;
	COORD coord;
	coord.X=x_coordinate;
	coord.Y=y_coordinate;
	hout= GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleCursorPosition(hout, coord);
}
void set_today(){
    time_t t = time(0);   // get time now
	struct tm * now = localtime( & t );
	year= (now->tm_year + 1900) ;
	month= (now->tm_mon + 1) ;
	day=now->tm_mday;
    set_cursor(55,22);
	cout<<"DATE: "<<year<<" - "<<month<<" - "<<day;
}
inline void set_border(){
	system("color 3f");
    HANDLE hout;
	COORD coord;
	hout= GetStdHandle(STD_OUTPUT_HANDLE);//screen status
    SetConsoleCursorPosition(hout, coord);//coursour positioning
		for(int i=0;i<=79;i++){
			if(i==1 || i== 78){
				for(int j=0;j<24;j++){
    				coord.X=i;
					coord.Y=j;
    				SetConsoleCursorPosition(hout, coord);//to select position pf cursor
					cout<<"*";//to assign '*' where the cursor is denoting
				}
			}
            coord.X=i;
	     	coord.Y=0;
			SetConsoleCursorPosition(hout, coord);
			cout<<"|";
		}
		for(int i=0;i<=79;i++){
			if(i==0 || i== 79){
				for(int j=1;j<24;j++){
					coord.X=i;
					coord.Y=j;
					SetConsoleCursorPosition(hout, coord);
					cout<<"*";
				}
			}
            coord.X=i;
    		SetConsoleCursorPosition(hout, coord);
			cout<<"|";
		}
		for(int i=2;i<=77;i++){
			for(int j=1;j<=22;j++){
				coord.X=i;
				coord.Y=j;
      	        SetConsoleCursorPosition(hout, coord);
	 			cout<<" ";
			}
		}
		set_today();
		set_cursor(10,24);
		printf("  SUBMITTED BY: SHITAL BABU LUITEL, BIKRAM MAJHI");
}
int admin::check_new(){
    ifstream record;
	record.open("data.dat");
	if(!record.is_open()){
		record.close();
		return 1;
	}
	else{
		record.close();
		return 0;
	}
}

int main(){
    system("color 3f");
    user obj;
	if(obj.admin::check_new()){
        set_border();
        y=10;
		set_cursor(25,y++);
		cout<<"***WELCOME FOR FIRST TIME***";
		wait(160);
		set_border();
		obj.admin::get_user_pw();//to detect users 1st use of application
	}
	do{
		set_border();
		display_anime(2);
		y=8;
		set_cursor(17,y);
		cout<<"*****************RETAILER MANAGEMENT***************";
		y++;
        set_cursor(25,++y);
        cout<<"Main Menu: ";
        set_cursor(18,++y);
		cout<<"              1. Add and Update";
		set_cursor(18,++y);
		cout<<"              2. View and Buy ";
		set_cursor(18,++y);
		cout<<"              3. Exit";
		set_cursor(25,++y);
		cout<<"Choice: ";
		cin>>choice;
		set_border();
		switch(choice[0]){
			case '1':
				if(obj.admin::check()){
					do{
						system("color 3f");
	     				set_border();
	     				display_anime(2);
						x=18;y=8;
						set_cursor(x,y++);
						cout<<"1. TO ENTER THE RECORD";
						set_cursor(x,y++);
						cout<<"2. TO UPDATE THE RECORD";
						set_cursor(x,y++);
						cout<<"3. BACK";
						y++;
	                    set_cursor(x,y++);
						cout<<"CHOICE: ";
						cin>>choice1;
						set_border();
						switch(choice1[0]){
							case '1':
	                                do{
                                        system("color 3f");
                                        set_border();
		                                display_anime(2);
										x=18;y=8;
										set_cursor(x,y++);
										cout<<"1. TO ADD IN EXISTING FILE.";
										set_cursor(x,y++);
										cout<<"2. TO ENTER NEW RECORD.";
										set_cursor(x,y++);
										cout<<"3. BACK";
										y++;
										set_cursor(x,y);
										cout<<"CHOICE: ";
										cin>>choice2;
										set_border();
										switch(choice2[0]){
											case '1':
												system("color 1f");
			                                    obj.admin::add_data();
			                                    break;
											case '2':
												system("color f1");
			                                    obj.admin::enter_data();
			                                    break;
											default:
												break;
										}
	                                }while(choice2[0]!='3');
									system("color 3f");
									break;
							case '2':
								obj.display_update();
								break;
						}
					}while(choice1[0]!='3');
				}
    			break;
			case '2':
				obj.check_list();
				break;
            case '3':
				set_border();
				display_anime(2);
				set_cursor(37,12);
				cout<<"Thank you";
				wait(15);
				return 0;
			default :
				break;
		}
	}while(1);
}
